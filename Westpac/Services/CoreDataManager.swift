//
//  CoreDataManager.swift
//  Westpac
//
//  Created by Nilson Filho on 15/08/20.
//  Copyright © 2020 Nilson Filho. All rights reserved.
//

import Foundation
import CoreData

class CoreDataManager {

    // MARK: Variables
    var managedObjectContext: NSManagedObjectContext

    // MARK: Init
    init(moc: NSManagedObjectContext) {
        self.managedObjectContext = moc
        self.managedObjectContext.mergePolicy = NSMergeByPropertyStoreTrumpMergePolicy
    }

    func updateEntities() {
        do {
            try self.managedObjectContext.save()
            print("Success")
        } catch {
            print("Error saving: \(error)")
        }
    }

    // MARK: getter and setter
    /*
     Function to be overriden by child class
     */
    func getObjects(id: String?) -> [NSManagedObject] {
        fatalError("Need to override getObjects")
    }

    func saveObjects(objectList: [Any], id: String?) {
        fatalError("Need to override saveObjects")
    }

}
