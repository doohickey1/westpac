//
//  WebService.swift
//  Westpac
//
//  Created by Nilson Filho on 15/08/20.
//  Copyright © 2020 Nilson Filho. All rights reserved.
//

import Foundation

class WebService {
    
    let baseURLString = "https://jsonplaceholder.typicode.com/"

    func get(directory: String, completion: @escaping (Result<Data, Error>) -> ()) {
        guard let url = URL(string: baseURLString + directory) else {
            fatalError("URL Problem")
        }

        URLSession.shared.dataTask(with: url) { data, response, error in
            if let error = error {
                print(error)
                DispatchQueue.main.async {
                    completion(.failure(error))
                }
            } else if let response = response as? HTTPURLResponse, response.statusCode == 200 {
                DispatchQueue.main.async {
                    completion(.success(data ?? Data()))
                }
            }
        }.resume()
    }
    
}
