//
//  UITableView+Extensions.swift
//  Westpac
//
//  Created by Nilson Filho on 15/08/20.
//  Copyright © 2020 Nilson Filho. All rights reserved.
//

import Foundation
import UIKit
import Bond
import ReactiveKit

extension UITableView {

    var selectedRow: Signal<IndexPath, Never> {
        return reactive.delegate.signal(for: #selector(UITableViewDelegate.tableView(_:didSelectRowAt:))) {
            (subject: PassthroughSubject<IndexPath, Never>, _: UITableView, indexPath: IndexPath) in
            subject.send(indexPath)
        }
    }
    
}
