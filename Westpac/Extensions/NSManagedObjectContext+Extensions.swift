//
//  NSManagedObjectContext+Extensions.swift
//  Westpac
//
//  Created by Nilson Filho on 15/08/20.
//  Copyright © 2020 Nilson Filho. All rights reserved.
//

import Foundation
import UIKit
import CoreData

extension NSManagedObjectContext {

    static var current: NSManagedObjectContext {
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        return appDelegate.persistentContainer.viewContext
    }
}
