//
//  LocalError.swift
//  Westpac
//
//  Created by Nilson Filho on 15/08/20.
//  Copyright © 2020 Nilson Filho. All rights reserved.
//

import Foundation

public protocol LocalErrorProtocol: Error {
    var errorString: String { get }
}

struct LocalError: LocalErrorProtocol {
    var errorString: String
}
