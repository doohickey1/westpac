//
//  PostsViewModel.swift
//  Westpac
//
//  Created by Nilson Filho on 15/08/20.
//  Copyright © 2020 Nilson Filho. All rights reserved.
//

import Foundation
import Bond
import ReactiveKit

class PostsViewModel {

    // MARK: Variables
    let postList = MutableObservableArray<Post>([])
    let loadingInProgress = Observable<Bool>(false)
    let errorMessage = PassthroughSubject<String, Never>()

    private var service = PostsRepository()

    // MARK: Init
    init() {
        getPosts()
    }

    // MARK: API Call
    func getPosts() {
        loadingInProgress.value = true
        service.getPosts(completion: { [weak self] result in
            switch result {
            case .success(let postArray):
                self?.postList.removeAll()
                self?.postList.insert(contentsOf: postArray, at: 0)
                self?.loadingInProgress.value = false
            case .failure(let error):
                self?.handleError(error)
                self?.loadingInProgress.value = false
            }
        })
    }

    // MARK: Private Functions
    private func handleError(_ error: Error) {
        if let localError = error as? LocalError {
            errorMessage.send(localError.errorString)
        } else {
            errorMessage.send(error.localizedDescription)
        }
    }
    
}
