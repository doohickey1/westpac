//
//  PostsRepository.swift
//  Westpac
//
//  Created by Nilson Filho on 15/08/20.
//  Copyright © 2020 Nilson Filho. All rights reserved.
//

import Foundation
import Network

class PostsRepository: PostsRepositoryProtocol {

    // MARK: PostsRepositoryProtocol
    func getPosts(completion: @escaping (Result<[Post], Error>) -> Void) {
        let monitor = NWPathMonitor()
        let queue = DispatchQueue(label: "Monitor")
        monitor.start(queue: queue)
        monitor.pathUpdateHandler = { [weak self] path in
            if path.status == .satisfied {
                self?.getPostsRemote(completion: completion)
            } else {
                self?.getPostsLocal(completion: completion)
            }
        }
        // we can cancel after check as we don't want to reload every time connection status change
        monitor.cancel()
    }

    // MARK: Private Functions
    private func getPostsRemote(completion: @escaping (Result<[Post], Error>) -> Void) {
        PostsRemoteRepository().getPosts(completion: { [weak self] result in
            switch result {
            case .success(let postArray):
                self?.saveToLocalRepository(postArray)
                completion(.success(postArray))
            case .failure(let error):
                completion(.failure(error))
            }
        })
    }

    private func getPostsLocal(completion: @escaping (Result<[Post], Error>) -> Void) {
        PostsLocalRepository().getPosts(completion: { result in
            switch result {
            case .success(let postArray):
                completion(.success(postArray))
            case .failure(let error):
                completion(.failure(error))
            }
        })
    }

    private func saveToLocalRepository(_ postArray: [Post]) {
        let localRepo = PostsLocalRepository()
        localRepo.saveObjects(objectList: postArray, id: nil)
    }

}
