//
//  PostsLocalRepository.swift
//  Westpac
//
//  Created by Nilson Filho on 15/08/20.
//  Copyright © 2020 Nilson Filho. All rights reserved.
//

import Foundation
import CoreData

class PostsLocalRepository: CoreDataManager {

    // MARK: Init
    init() {
        super.init(moc: NSManagedObjectContext.current)
    }

    // MARK: Overriden Functions
    override func getObjects(id: String?) -> [NSManagedObject] {
        var posts = [PostEntity]()
        let postRequest: NSFetchRequest<PostEntity> = PostEntity.fetchRequest()

        do {
            posts = try self.managedObjectContext.fetch(postRequest)
        } catch let error as NSError {
            print(error)
        }

        return posts
    }

    override func saveObjects(objectList: [Any], id: String?) {
        guard let postList = objectList as? [Post] else {
            return
        }
        for post in postList {
            savePost(post: post)
        }
    }

    // MARK: Private Functions
    private func savePost(post: Post) {
        let postEntity = NSEntityDescription.insertNewObject(forEntityName: "PostEntity", into: self.managedObjectContext)
        postEntity.setValue(post.id, forKey: "id")
        postEntity.setValue(post.userId, forKey: "userId")
        postEntity.setValue(post.title, forKey: "title")
        postEntity.setValue(post.body, forKey: "body")

        do {
            try self.managedObjectContext.save()
        } catch {
            print("Error saving: \(error)")
        }
    }
}

// MARK: PostsRepositoryProtocol
extension PostsLocalRepository: PostsRepositoryProtocol {

    func getPosts(completion: @escaping (Result<[Post], Error>) -> Void) {
        var postList = [Post]()
        guard let objectList = getObjects(id: nil) as? [PostEntity] else {return}
        if objectList.count > 0 {
            for object in objectList {
                let post = Post(id: Int(object.id),
                                userId: Int(object.userId),
                                title: object.title ?? "",
                                body: object.body ?? "")
                postList.append(post)
            }
            DispatchQueue.main.async {
                completion(.success(postList))
            }
        } else {
            DispatchQueue.main.async {
                completion(.failure(LocalError(errorString: NSLocalizedString("not connected and no cache", comment: ""))))
            }
        }
    }

}
