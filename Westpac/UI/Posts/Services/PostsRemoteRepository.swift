//
//  PostsRemoteRepository.swift
//  Westpac
//
//  Created by Nilson Filho on 15/08/20.
//  Copyright © 2020 Nilson Filho. All rights reserved.
//

import Foundation

class PostsRemoteRepository: PostsRepositoryProtocol {
    
    let postDirectoryURLString = "posts"
    
    func getPosts(completion: @escaping (Result<[Post], Error>) -> Void) {
        WebService().get(directory: postDirectoryURLString, completion: { result in
            switch result {
            case .success(let data):
                var postArray = [Post]()
                do {
                    postArray = try JSONDecoder().decode([Post].self, from: data)
                } catch {
                    print(error)
                }
                completion(.success(postArray))
            case .failure(let error):
                completion(.failure(error))
            }
        })
    }
}
