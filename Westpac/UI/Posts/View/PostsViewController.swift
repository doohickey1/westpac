//
//  PostsViewController.swift
//  Westpac
//
//  Created by Nilson Filho on 15/08/20.
//  Copyright © 2020 Nilson Filho. All rights reserved.
//

import Foundation
import UIKit

class PostsViewController: UIViewController {

    // MARK: IBOutlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    // MARK: Variables
    private var viewModel: PostsViewModel?
    private var refreshControl = UIRefreshControl()

    static let SEGUE_SHOW_COMMENTS = "ShowComments"

    // MARK: Lifecycle Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        title = NSLocalizedString("Posts", comment: "")
        addRefreshControl()
        setupBindings()
    }

    // MARK: Private Functions
    private func addRefreshControl() {
        refreshControl.addTarget(self, action: #selector(self.refresh(_:)), for: .valueChanged)
        tableView.refreshControl = refreshControl
    }

    private func setupBindings() {
        viewModel = PostsViewModel()
        bindCell()
        bindLoadingIndicator()
        bindErrorMessage()
        bindCellSelection()
    }

    private func bindCell() {
        viewModel?.postList.bind(to: tableView) { dataSource, indexPath, tableView in
            let cell = tableView.dequeueReusableCell(withIdentifier: "PostsCell", for: indexPath) as? PostsTableViewCell
            let post = dataSource[indexPath.row]
            cell?.postTitleLabel.text = post.title
            cell?.postDescriptionLabel.text = post.body
            return cell ?? UITableViewCell()
        }
    }

    private func bindLoadingIndicator() {
        viewModel?.loadingInProgress
            .map {!$0}
            .bind(to: activityIndicator.reactive.isHidden)
    }

    private func bindErrorMessage() {
        _ = viewModel?.errorMessage.observeNext { [weak self] error in
                let alertController = UIAlertController(title: NSLocalizedString("Something Went Wrong", comment: ""),
                                                        message: error,
                                                        preferredStyle: .alert)
                self?.present(alertController, animated: true, completion: nil)

                let actionOk = UIAlertAction(title: NSLocalizedString("OK", comment: ""),
                                             style: .default,
                                             handler: { action in alertController.dismiss(animated: true, completion: nil) })

                alertController.addAction(actionOk)
        }
    }

    private func bindCellSelection() {
        _ = tableView.selectedRow.observeNext { [weak self] indexPath in
            self?.tableView.deselectRow(at: indexPath, animated: false)
            let post = self?.viewModel?.postList[indexPath.row]
            self?.performSegue(withIdentifier: PostsViewController.SEGUE_SHOW_COMMENTS, sender: post?.id)
        }
    }

    // MARK: Actions
    @objc func refresh(_ sender: AnyObject) {
        viewModel?.getPosts()
        refreshControl.endRefreshing()
    }

}

// MARK: Segue Functions
extension PostsViewController {
    // Unwind from CommentsViewController
    @IBAction func unwindToPosts(_ unwindSegue: UIStoryboardSegue) {}

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == PostsViewController.SEGUE_SHOW_COMMENTS {
            if let postID = sender as? Int, let navViewController = segue.destination as? UINavigationController {
                guard let controller = navViewController.topViewController as? CommentsViewController else {
                    return
                }
                
                let commentsViewModel = CommentsViewModel(postID: postID)
                controller.viewModel = commentsViewModel
            }
        }
    }
}
