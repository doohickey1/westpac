//
//  CommentsTableViewCell.swift
//  Westpac
//
//  Created by Nilson Filho on 15/08/20.
//  Copyright © 2020 Nilson Filho. All rights reserved.
//

import UIKit

class CommentsTableViewCell: UITableViewCell {

    // MARK: IBOutlet
    @IBOutlet weak var commentNameLabel: UILabel!
    @IBOutlet weak var commentEmailLabel: UILabel!
    @IBOutlet weak var commentBodyLabel: UILabel!

    // MARK: Lifecycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
