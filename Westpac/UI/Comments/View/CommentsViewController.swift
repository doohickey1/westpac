//
//  CommentsViewController.swift
//  Westpac
//
//  Created by Nilson Filho on 15/08/20.
//  Copyright © 2020 Nilson Filho. All rights reserved.
//

import UIKit

class CommentsViewController: UIViewController {

    // MARK: IBOutlet
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!

    // MARK: Variables
    var viewModel: CommentsViewModel?

    // MARK: Lifecycle Functions
    override func viewDidLoad() {
        super.viewDidLoad()
        title = NSLocalizedString("Comments", comment: "")
        setupBindings()
    }

    // MARK: Private Functions
    private func setupBindings() {
        bindCell()
        bindLoadingIndicator()
        bindErrorMessage()
    }

    private func bindCell() {
        viewModel?.commentList.bind(to: tableView) { dataSource, indexPath, tableView in
            let cell = tableView.dequeueReusableCell(withIdentifier: "CommentsCell", for: indexPath) as? CommentsTableViewCell
            let comment = dataSource[indexPath.row]
            cell?.commentNameLabel.text = comment.name
            cell?.commentEmailLabel.text = comment.email
            cell?.commentBodyLabel.text = comment.body
            return cell ?? UITableViewCell()
        }
    }

    private func bindLoadingIndicator() {
        viewModel?.loadingInProgress
            .map {!$0}
            .bind(to: activityIndicator.reactive.isHidden)
    }

    private func bindErrorMessage() {
        _ = viewModel?.errorMessage.observeNext { [weak self] error in
                let alertController = UIAlertController(title: NSLocalizedString("Something Went Wrong", comment: ""),
                                                        message: error,
                                                        preferredStyle: .alert)
                self?.present(alertController, animated: true, completion: nil)

                let actionOk = UIAlertAction(title: NSLocalizedString("OK", comment: ""),
                                             style: .default,
                                             handler: { action in alertController.dismiss(animated: true, completion: nil) })

                alertController.addAction(actionOk)
        }
    }

}
