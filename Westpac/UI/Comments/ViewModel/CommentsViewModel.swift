//
//  CommentsViewModel.swift
//  Westpac
//
//  Created by Nilson Filho on 15/08/20.
//  Copyright © 2020 Nilson Filho. All rights reserved.
//

import Foundation
import Bond
import ReactiveKit

class CommentsViewModel {

    // MARK: Variables
    let commentList = MutableObservableArray<Comment>([])
    let loadingInProgress = Observable<Bool>(false)
    let errorMessage = PassthroughSubject<String, Never>()

    private let service = CommentsRepository()
    private var postID: Int

    // MARK: Init
    init(postID: Int) {
        self.postID = postID
        getComments()
    }

    // MARK: API Call
    func getComments() {
        loadingInProgress.value = true
        service.getComments(postID: String(postID), completion: { [weak self] result in
            switch result {
            case .success(let commentArray):
                self?.commentList.removeAll()
                self?.commentList.insert(contentsOf: commentArray, at: 0)
                self?.loadingInProgress.value = false
            case .failure(let error):
                self?.handleError(error)
                self?.loadingInProgress.value = false
            }
        })
    }

    // MARK: Private Functions
    private func handleError(_ error: Error) {
        if let localError = error as? LocalError {
            errorMessage.send(localError.errorString)
        } else {
            errorMessage.send(error.localizedDescription)
        }
    }
}
