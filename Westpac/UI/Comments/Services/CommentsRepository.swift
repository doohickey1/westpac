//
//  CommentsRepository.swift
//  Westpac
//
//  Created by Nilson Filho on 15/08/20.
//  Copyright © 2020 Nilson Filho. All rights reserved.
//

import Foundation
import Network

class CommentsRepository: CommentsRepositoryProtocol {

    // MARK: CommentsRepositoryProtocol
    func getComments(postID: String, completion: @escaping (Result<[Comment], Error>) -> Void) {
        let monitor = NWPathMonitor()
        let queue = DispatchQueue(label: "Monitor")
        monitor.start(queue: queue)
        monitor.pathUpdateHandler = { [weak self] path in
            if path.status == .satisfied {
                self?.getCommentsRemote(postID: postID, completion: completion)
            } else {
                self?.getCommentsLocal(postID: postID, completion: completion)
            }
        }
        // we can cancel after check as we don't want to reload every time connection status change
        monitor.cancel()
    }

    // MARK: Private Functions
    private func getCommentsRemote(postID: String, completion: @escaping (Result<[Comment], Error>) -> Void) {
        CommentsRemoteRepository().getComments(postID: postID, completion: { [weak self] result in
            switch result {
            case .success(let commentArray):
                self?.saveToLocalRepository(commentArray, id: postID)
                completion(.success(commentArray))
            case .failure(let error):
                completion(.failure(error))
            }
        })
    }

    private func getCommentsLocal(postID: String, completion: @escaping (Result<[Comment], Error>) -> Void) {
        CommentsLocalRepository().getComments(postID: postID, completion: {
            result in
            switch result {
            case .success(let postArray):
                completion(.success(postArray))
            case .failure(let error):
                completion(.failure(error))
            }
        })
    }

    private func saveToLocalRepository(_ commentArray: [Comment], id: String) {
        let localRepo = CommentsLocalRepository()
        localRepo.saveObjects(objectList: commentArray, id: id)
    }
    
}
