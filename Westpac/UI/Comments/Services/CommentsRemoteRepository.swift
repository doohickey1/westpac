//
//  CommentsRemoteRepository.swift
//  Westpac
//
//  Created by Nilson Filho on 15/08/20.
//  Copyright © 2020 Nilson Filho. All rights reserved.
//

import Foundation

class CommentsRemoteRepository: CommentsRepositoryProtocol {
    
    let commentDirectoryURLString = "comments?postId="

    func getComments(postID: String, completion: @escaping (Result<[Comment], Error>) -> Void) {
        let directoryUrlString = commentDirectoryURLString + postID
        WebService().get(directory: directoryUrlString, completion: { result in
            switch result {
            case .success(let data):
                var commentArray = [Comment]()
                do {
                    commentArray = try JSONDecoder().decode([Comment].self, from: data)
                } catch {
                    print(error)
                }
                completion(.success(commentArray))
            case .failure(let error):
                completion(.failure(error))
            }
        })
    }
}
