//
//  CommentsLocalRepository.swift
//  Westpac
//
//  Created by Nilson Filho on 15/08/20.
//  Copyright © 2020 Nilson Filho. All rights reserved.
//

import Foundation
import CoreData

class CommentsLocalRepository: CoreDataManager {

    // MARK: Init
    init() {
        super.init(moc: NSManagedObjectContext.current)
    }

    // MARK: Overriden Functions
    override func saveObjects(objectList: [Any], id: String?) {
        var commentsArray = Set<CommentEntity>([])
        guard let commentsList = objectList as? [Comment] else {
            return
        }
        for comments in commentsList {
            let commentEntity = getCommentEntity(comments: comments)
            commentsArray.insert(commentEntity)
        }
        
        let commentsArrayEntity = NSEntityDescription.insertNewObject(forEntityName: "CommentArrayEntity", into: self.managedObjectContext)
        commentsArrayEntity.setValue(commentsArray, forKey: "commentEntity")
        commentsArrayEntity.setValue(id, forKey: "id")

        do {
            try self.managedObjectContext.save()
            print("Saved")
        } catch {
            print("Error saving: \(error)")
        }
    }

    override func getObjects(id: String?) -> [NSManagedObject] {
           var comments = [CommentEntity]()
           let commentRequest: NSFetchRequest<CommentArrayEntity> = CommentArrayEntity.fetchRequest()
           let predicate = NSPredicate(format: "id = %@", id ?? "")
           commentRequest.predicate = predicate

           do {
               let commentArrayEntity = try self.managedObjectContext.fetch(commentRequest)
               if commentArrayEntity.count > 0 {
                   if let set = commentArrayEntity[0].commentEntity, let commentList = set.allObjects as? [CommentEntity] {
                       comments.append(contentsOf: commentList.sorted(by: {$0.id < $1.id}))
                   }
               }
           } catch let error as NSError {
               print(error)
           }

           return comments
       }

    // MARK: Private Functions
    private func getCommentEntity(comments: Comment) -> CommentEntity {
        if let commentsEntity = NSEntityDescription.insertNewObject(forEntityName: "CommentEntity", into: self.managedObjectContext) as? CommentEntity {
            commentsEntity.setValue(comments.id, forKey: "id")
            commentsEntity.setValue(comments.postId, forKey: "postId")
            commentsEntity.setValue(comments.name, forKey: "name")
            commentsEntity.setValue(comments.body, forKey: "body")
            commentsEntity.setValue(comments.email, forKey: "email")
            return commentsEntity
        } else {
            fatalError("Coudn't get CommentEntity")
        }
    }
}

// MARK: CommentsRepositoryProtocol
extension CommentsLocalRepository: CommentsRepositoryProtocol {

    func getComments(postID: String, completion: @escaping (Result<[Comment], Error>) -> Void) {
        var commentList = [Comment]()
        guard let objectList = getObjects(id: postID) as? [CommentEntity] else {return}
        if objectList.count > 0 {
            for object in objectList {
                let comment = Comment(postId: Int(object.postId),
                                      id: Int(object.id),
                                      name: object.name ?? "",
                                      email: object.email ?? "",
                                      body: object.body ?? "")
                commentList.append(comment)
            }
            DispatchQueue.main.async {
                completion(.success(commentList))
            }
        } else {
            DispatchQueue.main.async {
                completion(.failure(LocalError(errorString: NSLocalizedString("not connected and no cache", comment: ""))))
            }
        }
    }

}
