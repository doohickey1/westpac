//
//  CommentsRepositoryProtocol.swift
//  Westpac
//
//  Created by Nilson Filho on 15/08/20.
//  Copyright © 2020 Nilson Filho. All rights reserved.
//

import Foundation

protocol CommentsRepositoryProtocol {
    func getComments(postID: String, completion: @escaping (Result<[Comment], Error>) -> Void)
}
