# Westpac

## Example Application

To run the example project, clone the repo "git clone https://doohickey1@bitbucket.org/doohickey1/westpac.git", and run normally, as it uses Swift Package Manager no extra setup needed.

## Requirements

i- OS 13.0 and above.

## Tools Used to create

- xCode version 11.6
- Swift 5
- Bond framework to connect ViewModel

## Design Pattern

- MVVM

## Author

- Nilson Santos Filho, nilson@mane.app

## License

Westpac is available under the MIT license. See the LICENSE file for more info.

